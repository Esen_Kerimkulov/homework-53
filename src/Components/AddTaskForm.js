import React, { Component } from 'react'

class AddTaskForm extends Component {
    createTasks = item => {
        return (
            <li key={item.key}>
                {item.text}
                <button className="btn" onClick={() => this.props.deleteItem(item.key)}>{'\u2718'}</button>
            </li>
        )
    };
    render() {
        const todoEntries = this.props.entries;
        const listItems = todoEntries.map(this.createTasks);

        return <ul className="theList">{listItems}</ul>
    }
}

export default AddTaskForm;